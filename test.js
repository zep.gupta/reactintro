let things = [{ a: 1 }, { a: 2 }, { a: 3 }];

function inc() {
  this.a++;
}

things.forEach(t => inc.call(t));

things.forEach(t => console.log(t.a));
//2
//3
//4

export default inc;

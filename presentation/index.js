// Import React
import React from "react";
// Import Spectacle Core tags
import {
  Deck,
  Code,
  CodePane,
  Heading,
  Image,
  ListItem,
  List,
  Link,
  Slide,
  Text,
  SlideSet
} from "spectacle";
import CodeSlide from "spectacle-code-slide";
import Todo from "../components/todo";
import Counter from "../components/Counter";
// Import theme
import createTheme from "spectacle/lib/themes/default";
import NameInput from "../components/nameInput";
import MaterialUIEx from "../components/materialUI";
import "./codeTheme.css";

// Require CSS
require("normalize.css");

const theme = createTheme(
  {
    primary: "white",
    secondary: "#1F2022",
    tertiary: "#03A9FC",
    quaternary: "#CECECE"
  },
  {
    primary: "Montserrat",
    secondary: "Helvetica"
  }
);

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck transition={["zoom", "slide"]} transitionDuration={500} theme={theme}>
        <Slide transition={["zoom"]} bgColor="secondary">
          <Heading size={4} caps lineHeight={1} textColor="tertiary">
            An Intro to React.js
          </Heading>
          <Heading size={6} caps lineHeight={1} textColor="tertiary">
            (For Developers)
          </Heading>
          <Text margin="10px 0 0" textColor="primary">
            By Dhirj Gupta
          </Text>
          <Text margin="10px 0 0" textSize="16px" textColor="primary">
            linkedin.com/in/dhirj-gupta/
          </Text>
        </Slide>
        {/* js from java */}
        <Slide transition={["slide"]} bgColor={"secondary"}>
          <Heading fit caps textColor="tertiary">
            Some Javascript Fundamentals!!
          </Heading>
        </Slide>
        {/* Getting Started */}
        <Slide transition={["zoom"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            Getting Started
          </Heading>
          <List>
            <ListItem textColor="primary">
              1. Install{" "}
              <Link href="nodejs.org" target="_blank">
                node.js
              </Link>{" "}
            </ListItem>
            <ListItem textColor="primary">
              2. Download{" "}
              <Link href="https://code.visualstudio.com/" target="_blank">
                Visual Studio Code
              </Link>{" "}
            </ListItem>
            <ListItem textColor="primary">3. Run a js file with "node [file]"</ListItem>
          </List>
        </Slide>
        {/* let and const */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            var vs let vs const
          </Heading>
          <List textColor="primary">
            <ListItem textSize="25px">forget var</ListItem>
            <ListItem textSize="25px">use let for primitive values you want to change</ListItem>
            <ListItem textSize="25px">use const for unchanging primitives and pointers</ListItem>
          </List>
          <CodePane
            lang="js"
            source={require("raw-loader!../assets/var/1.example")}
            textSize="18px"
          />
        </Slide>
        <SlideSet>
          {/* javascript objects*/}
          <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
            <Heading textColor="tertiary" size={5}>
              Objects in Javascript
            </Heading>
            <List textColor="primary">
              <ListItem textSize="20px">Javascript is NOT an object-oriented language!</ListItem>
              <ListItem textSize="20px">In JS land, objects are just data-structures</ListItem>
              <ListItem textSize="20px">JSON = JavaScript Object Notation</ListItem>
              <ListItem textSize="20px">
                Working with JS objects is similar to working with JSON
              </ListItem>
            </List>
            <CodePane
              lang="js"
              source={require("raw-loader!../assets/objects/1.example")}
              textSize="17px"
            />
          </Slide>
          {/* function slides */}
          <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
            <Heading textColor="tertiary" size={5}>
              JS Functions - 1
            </Heading>
            <CodePane
              lang="js"
              source={require("raw-loader!../assets/functions/1.example")}
              textSize="20px"
            />
          </Slide>
          <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
            <Heading textColor="tertiary" size={5}>
              JS Functions - 2
            </Heading>
            <Text textSize="25px" textColor="primary">
              Functions can be passed to (higher-order functions) and returned from other functions
            </Text>
            <CodePane
              lang="js"
              source={require("raw-loader!../assets/functions/2.example")}
              textSize="18px"
            />
          </Slide>
          <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
            <Heading textColor="tertiary" size={5}>
              JS Functions - 3
            </Heading>
            <Text textColor="primary" textSize="25px">
              Many native JS functions work this way (Higher-Order Functions). The methods below
              exist on all arrays.
            </Text>
            <CodePane
              lang="js"
              source={require("raw-loader!../assets/functions/3.example")}
              textSize="20px"
            />
          </Slide>
          <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
            <Heading textColor="tertiary" size={5}>
              Scope and Closure
            </Heading>
            <List textColor="primary">
              <ListItem textSize="30px" padding="10px 0 10px 0">
                how to manage your variables and protect them from unwanted access
              </ListItem>
              <ListItem textSize="30px" padding="10px 0 10px 0">
                The Book series{" "}
                <Link href="https://github.com/getify/You-Dont-Know-JS/blob/2nd-ed/get-started/README.md">
                  "You Dont Know JS"
                </Link>{" "}
                explains it well
              </ListItem>
              <ListItem textSize="30px" padding="10px 0 10px 0">
                A tip to get started though - things defined inside a function cannot be accessed
                from outside the function unless returned from that function's call
              </ListItem>
            </List>
          </Slide>
          <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
            <Heading textColor="tertiary" size={5}>
              ES6 Modules
            </Heading>
            <Text textColor="primary" textSize="25px">
              Spreading your code across multiple files...
            </Text>
            <CodePane
              lang="js"
              source={require("raw-loader!../assets/modules/1.example")}
              textSize="17px"
            />
          </Slide>
        </SlideSet>
        {/* Async */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            Async in JS - 1
          </Heading>
          <List textColor="primary">
            <ListItem textSize="25px">
              Fundamentally, Javascript is single threaded, non-blocking, and event driven. Heres a{" "}
              <Link href="https://codeburst.io/how-node-js-single-thread-mechanism-work-understanding-event-loop-in-nodejs-230f7440b0ea">
                link
              </Link>{" "}
              with more info
            </ListItem>
            <ListItem textSize="25px">
              The way async code has evolved a great deal over the past years
            </ListItem>
            <ListItem textSize="25px">Originally though, it was managed via callbacks</ListItem>
          </List>
          <CodePane
            lang="js"
            source={require("raw-loader!../assets/async/1.example")}
            textSize="18px"
          />
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            Async in JS - 2
          </Heading>
          <CodePane
            lang="js"
            source={require("raw-loader!../assets/async/2.example")}
            textSize="18px"
          />
          <Text textSize="25px" textColor="primary">
            ...this inevitably led to "Callback Hell"
          </Text>
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            Async in JS - 3
          </Heading>
          <Text textSize="25px" textColor="primary">
            ...So to help they created promises, which chain
          </Text>
          <CodePane
            lang="js"
            source={require("raw-loader!../assets/async/3.example")}
            textSize="17px"
          />
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            Async in JS - 4
          </Heading>
          <Text textSize="25px" textColor="primary">
            ...And finally we got async/await - basically syntactic sugar for promises
          </Text>
          <CodePane
            lang="js"
            source={require("raw-loader!../assets/async/4.example")}
            textSize="20px"
          />
        </Slide>
        {/* classes */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            Classes in JS
          </Heading>
          <List>
            <ListItem textSize="20px" textColor="primary">
              They DO NOT implement an object-oriented inheritance.
            </ListItem>
            <ListItem textSize="20px" textColor="primary">
              Syntactic sugar for JS prototype inheritance system.
            </ListItem>
          </List>
          <CodePane
            lang="js"
            source={require("raw-loader!../assets/classes/1.example")}
            textSize="17px"
          />
        </Slide>
        {/* this */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            "this" in Javascript - 1
          </Heading>
          <CodePane
            lang="js"
            source={require("raw-loader!../assets/this/1.example")}
            textSize="18px"
          />
          <Text textColor="primary" textSize="25px">
            you can use the bind function to get a copy of a function with your choice of "this"
            context
          </Text>
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            "this" in Javascript - 2
          </Heading>
          <CodePane
            lang="js"
            source={require("raw-loader!../assets/this/2.example")}
            textSize="18px"
          />
          <Text textColor="primary" textSize="25px">
            "call" calls a specified function with your choice of "this" context
          </Text>
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            Quirks of Javascript
          </Heading>
          <List>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              Here are some quirks, and 80/20 rule suggestions to get through them
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              JS has two ways of checking equality, <Code textSize="20px">==</Code> and{" "}
              <Code textSize="20px">===</Code>. Use <Code textSize="20px">===</Code>.
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              <Code textSize="20px">0</Code> and <Code textSize="20px">''</Code> are falsey
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              Object properties can also be accessed via{" "}
              <Code textSize="20px">myObj['propName']</Code> notation. This means you can access
              object values using property names stored in variables.
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              Becareful with <Code textSize="20px">this</Code>. <Code textSize="20px">this</Code>{" "}
              does not always represent what you think it means.
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              If you use <Code textSize="20px">typeof</Code> on an array it will say "object"
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              Never mess with the prototypes of things you didn't build
            </ListItem>
          </List>
        </Slide>
        {/* On to React */}
        <Slide transition={["slide"]} bgColor={"secondary"}>
          <Heading fit caps textColor="tertiary">
            On to React!!
          </Heading>
        </Slide>
        {/* npm init */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            Starting A Project
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              Pick a location and in terminal type: "npm init"
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              Complete the prompts, instantiating your project
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              enter "npx create-react-app [app name]"
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              Complete the prompts (try out Typescript!)
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              "npm run start"
            </ListItem>
          </List>
        </Slide>
        {/* What is react */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - What is React?
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              A UI library that makes dealing with the browser view awesome.
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              Enables greater separation of concerns and modularity
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              Allows us to define "view as fn of states". In fact if you:
            </ListItem>
            <Text textSize="25px" textColor="primary" margin="10px 0 0 2em ">
              1. define how the view should look, given possible states
            </Text>
            <Text textSize="25px" textColor="primary" margin="10px 0 0 2em ">
              2. define an initial state
            </Text>
            <Text textSize="25px" textColor="primary" margin="10px 0 0 2em ">
              3. define how to update state
            </Text>
            <Text textSize="25px" textColor="primary" margin="10px 0 0 2em ">
              4. hook update mechanisms into your view definitions
            </Text>
            <Text textSize="25px" textColor="primary" margin="10px 0 0 1em ">
              React takes care of the rendering stuff for you
            </Text>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              Comes with a nice JSX syntax (simulates writing html in js)
            </ListItem>
          </List>
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - What is React? 2
          </Heading>
          <CodePane
            lang="jsx"
            source={require("raw-loader!../assets/react/simpleExample.example")}
            textSize="20px"
          />
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - Virtual Dom
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary">
              In React, for every DOM object, there is also a "virtual DOM object"
            </ListItem>
            <ListItem textSize="25px" textColor="primary">
              When state changes, React efficiently updates the view.
            </ListItem>
            <Text textSize="25px" textColor="primary" style={{ marginLeft: "2em" }}>
              1. The virtual DOM gets updated.
            </Text>
            <Text textSize="25px" textColor="primary" style={{ marginLeft: "2em" }}>
              2. The virtual DOM gets compared to what it looked like before you updated it.
            </Text>
            <Text textSize="25px" textColor="primary" style={{ marginLeft: "2em" }}>
              3. Only the affected parts of the DOM, get updated. (Screen update)
            </Text>
          </List>
          <Image height="350px" src={require("../assets/react/vDom.png")} />
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - Ecosystem
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              React was originally a view layer only solution (until hooks recently)
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              A huge open-source community has grown around React and JS with tons of awesome
              libraries
            </ListItem>
            <ListItem
              style={{ paddingLeft: "2em" }}
              textSize="25px"
              textColor="primary"
              margin="10px 0 0 0 "
            >
              Routing:
              <List style={{ paddingLeft: "2em" }}>
                <ListItem textSize="20px" textColor="secondary" margin="10px 0 0 0 ">
                  <Link href="https://github.com/ReactTraining/react-router" target="_blank">
                    React-Router
                  </Link>
                </ListItem>
              </List>
            </ListItem>
            <ListItem
              style={{ paddingLeft: "2em" }}
              textSize="25px"
              textColor="primary"
              margin="10px 0 0 0 "
            >
              State Management:
              <List style={{ paddingLeft: "2em" }}>
                <ListItem textSize="20px" textColor="secondary" margin="10px 0 0 0 ">
                  <Link href="https://github.com/reduxjs/redux" target="_blank">
                    Redux
                  </Link>
                </ListItem>
                <ListItem textSize="20px" textColor="secondary" margin="10px 0 0 0 ">
                  <Link href="https://redux-saga.js.org/" target="_blank">
                    Redux-Sagas
                  </Link>
                </ListItem>
                <ListItem textSize="20px" textColor="secondary" margin="10px 0 0 0 ">
                  <Link href="https://github.com/reduxjs/reselect" target="_blank">
                    Reselect
                  </Link>
                </ListItem>
              </List>
            </ListItem>
            <ListItem
              style={{ paddingLeft: "2em" }}
              textSize="25px"
              textColor="primary"
              margin="10px 0 0 0 "
            >
              Components:
              <List style={{ paddingLeft: "2em" }}>
                <ListItem textSize="20px" textColor="secondary" margin="10px 0 0 0 ">
                  <Link href="https://material-ui.com/" target="_blank">
                    Material-UI
                  </Link>
                </ListItem>
                <ListItem textSize="20px" textColor="secondary" margin="10px 0 0 0 ">
                  <Link href="https://react-bootstrap.github.io/" target="_blank">
                    React Bootstrap
                  </Link>
                </ListItem>
                <ListItem textSize="20px" textColor="secondary" margin="10px 0 0 0 ">
                  <Link href="https://ant.design" target="_blank">
                    Ant Design
                  </Link>
                </ListItem>
              </List>
            </ListItem>
          </List>
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - Example
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 30px 0 ">
              Material-UI is a react library with dozens of UI components, based on Google's UX
              guides
            </ListItem>
            <MaterialUIEx />
          </List>
        </Slide>
        {/* components */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - 2 kinds of components
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 40px ">
              1) Functional components - now with native state management via hooks
            </ListItem>
            <CodePane
              lang="jsx"
              source={require("raw-loader!../assets/react/4.example")}
              textSize="20px"
            />
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 40px ">
              2) Class components - previously the only way to have local state / shared state
            </ListItem>
          </List>
          <Text textSize="25px" textColor="tertiary" style={{ marginLeft: "2em" }}>
            With "Hooks", Functional components can do everything class components can with less
            code, and provide additional functionality as well. Classes will remain though for
            legacy purposes. So just focus on functional components for now.
          </Text>
        </Slide>
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - 2 kinds of components
          </Heading>
          <Counter />
        </Slide>
        {/* composition */}
        <CodeSlide
          transition={["slide"]}
          lang="js"
          bgColor="secondary"
          textSize="20px"
          code={require("raw-loader!../assets/react/6.example")}
          ranges={[
            // { loc: [0, 10], title: "Composition" },
            { loc: [0, 0], title: "Component Composition" },
            { loc: [0, 17], title: "Input Component - With TS!!" },
            { loc: [2, 7], title: "Input Component - TS Interface" },
            { loc: [8, 9], title: "Input Component - TS Declaring Type" },
            { loc: [9, 15], title: "Input Component - Render" },
            { loc: [17, 22], title: "Form Component - State Management" },
            { loc: [21, 36], title: "Form Component - Putting it all together" }
          ]}
        />
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - Example
          </Heading>
          <NameInput />
        </Slide>
        {/* flux */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - 5 - Data Flow
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              React is a view layer
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              "View as a function of State"
            </ListItem>
          </List>
          <Image height="500px" src={require("../assets/react/actionUpStateDown.png")} />
        </Slide>
        {/* Context API */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - Hooks
          </Heading>
          <Text margin="40px 0 20px 0" textSize="25px" textColor="tertiary">
            <Link href="https://reactjs.org/docs/hooks-intro.html" target="_blank">
              Hooks
            </Link>{" "}
            let you manage state, lifecycle, and more in functional components.
          </Text>
          <List>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              useState - manage some simple state
              {/* <CodePane lang="jsx"
                source={require("raw-loader!../assets/react/useState.example")}
              textSize="20px" /> */}
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              useReducer - manage complex state
              {/* <CodePane lang="jsx"
                source={require("raw-loader!../assets/react/useReducer.example")}
              textSize="20px" /> */}
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              useEffect - manage component updates, lifecycle, async and more
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              useContext - manage state throughout component trees (regardless of depth)
            </ListItem>
          </List>
        </Slide>
        {/* <CodeSlide
          transition={["slide"]}
          lang="js"
          bgColor="tertiary"
          textSize="20px"
          code={require("raw-loader!../assets/contextApi/1.example")}
          ranges={[
            { loc: [0, 0], title: "Context API" },
            { loc: [2, 6] },
            { loc: [7, 13] },
            { loc: [13, 14] },
            { loc: [15, 20] },
            { loc: [21, 26] },
            { loc: [27, 41] },
            { loc: [43, 52] },
            { loc: [53, 55] },
            { loc: [55, 73] }
          ]}
        /> */}
        {/* example 2 */}
        {/* <CodeSlide
          transition={["slide"]}
          lang="js"
          bgColor="tertiary"
          textSize="20px"
          code={require("raw-loader!../assets/react/7.example")}
          ranges={[
            // { loc: [0, 10], title: "Composition" },
            { loc: [0, 0], title: "ToDo App" },
            { loc: [3, 8] },
            { loc: [9, 12] },
            { loc: [13, 22] },
            { loc: [25, 31] },
            { loc: [33, 37] },
            { loc: [38, 41] },
            { loc: [42, 48] },
            { loc: [49, 62] }
          ]}
        />
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - 6 - Todo App
          </Heading>
          <Todo />
        </Slide> */}
        {/* React Router */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - React Router
          </Heading>
          <List>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              If you want a SPA with multiple routes, you need a routing library
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              React Router (v4) is one of the most widely used
            </ListItem>
            <CodePane
              margin="30px 0 0 0"
              lang="jsx"
              source={require("raw-loader!../assets/3rdParty/1.example")}
              textSize="18px"
            />
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              The Router component actually implements Context to "provide" browser route state info
              and means of updating it.
            </ListItem>
            <ListItem textSize="20px" textColor="primary" margin="10px 0 0 0 ">
              The Switch component "consumes" that state to figure out which of it's children to
              render.
            </ListItem>
          </List>
        </Slide>
        {/* Ajax */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - Ajax Libraries
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              There are many libraries for making ajax calls
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              Popular libraries include superagent and axios
            </ListItem>
            <CodePane
              margin="30px 0 0 0"
              lang="jsx"
              source={require("raw-loader!../assets/3rdParty/2.example")}
              textSize="20px"
            />
          </List>
        </Slide>
        <CodeSlide
          transition={["slide"]}
          lang="js"
          bgColor="secondary"
          textSize="20px"
          code={require("raw-loader!../assets/3rdParty/2_1.example")}
          ranges={[
            // { loc: [0, 10], title: "Composition" },
            { loc: [0, 0], title: "Ajax in React Example" },
            { loc: [1, 2], title: "Import ajax fns" },
            { loc: [2, 3], title: "Import my components" },
            { loc: [4, 9], title: "Setup your state" },
            { loc: [8, 13], title: "Ajax on init" },
            { loc: [13, 17], title: "Ajax on Selection" },
            { loc: [18, 31], title: "Render Movies" },
            { loc: [31, 42], title: "Render Cast" }
          ]}
        />
        {/* Component Libraries */}
        {/* 
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            React - 11 - Organizing you Project
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              Name your main exports index.js(x) inside their own folders
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              /src/components/myFirstComponent/index.jsx ...
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              Can then be imported just by referencing the myFirstComponent folder
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              This is because if your file reference is a folder, it'll look for an index by default
            </ListItem>
          </List>
        </Slide> */}
        <Slide transition={["slide"]} bgColor={"secondary"} align="center flex-start">
          <Heading textColor="tertiary" size={5}>
            Recommended Resources
          </Heading>
          <List>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              <Link href="https://codesandbox.io/s/react-ts" target="_blank">
                React-TS Coding Sandbox
              </Link>{" "}
              - Try-out React with Typescript features enabled, in the browser, right now
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              <Link href="https://htmlcheatsheet.com/" target="_blank">
                HTML
              </Link>{" "}
              - A quick reference for what all types of HTML tags exist
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              <Link
                href="https://www.dummies.com/web-design-development/site-development/common-css-attributes/"
                target="_blank"
              >
                CSS
              </Link>{" "}
              - Some common css attributes
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              <Link
                href="https://www.dummies.com/web-design-development/site-development/common-css-attributes/"
                target="_blank"
              >
                CSS FlexBox
              </Link>{" "}
              - 80 / 20 rule - use this for layout. Also look up fixed, absolute, and relative
              positioning
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              <Link href="https://github.com/getify/You-Dont-Know-JS" target="_blank">
                You Don't Know JS (YDJS)
              </Link>{" "}
              - so don't forget to learn more about it
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              <Link href="https://lodash.com" target="_blank">
                Lodash.js
              </Link>{" "}
              - Javascript Utility Library. It'll make your life easier.
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              <Link href="https://www.typescriptlang.org/" target="_blank">
                Typescript
              </Link>{" "}
              - Typed superset of javascript
            </ListItem>
            <ListItem textSize="25px" textColor="primary" margin="10px 0 0 0 ">
              <Link href="https://github.com/facebook/create-react-app" target="_blank">
                CreateReactApp
              </Link>{" "}
              - Starter kit that set's you up with a local dev environment with hot-reloading right
              away
            </ListItem>
          </List>
        </Slide>
      </Deck>
    );
  }
}

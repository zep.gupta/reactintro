import React from "react";

export default class NameInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first: "",
      last: ""
    };
  }
  handleChange = field => e => {
    const newState = Object.assign({}, this.state);
    newState[field] = e.target.value;
    this.setState(newState);
  };
  render() {
    return (
      <div style={{ display: "flex", flexDirection: "column" }}>
        <Input value={this.state.first} label="First" handleChange={this.handleChange("first")} />
        <Input value={this.state.last} label="Last" handleChange={this.handleChange("last")} />
      </div>
    );
  }
}

function Input(props) {
  return (
    <div style={{ display: "flex", flexDirection: "column", alignItems: "start" }}>
      <h4 style={{ color: "white" }}>{props.label}</h4>
      <input value={props.value} onChange={props.handleChange} />
    </div>
  );
}

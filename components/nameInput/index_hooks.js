import * as React from "react";


function Input(props) {
  return (
    <div style={{display: 'flex'}}>
      <span style={{fontWeight: 'bold'}}>{props.label}</span>
      <input value={props.value} onChange={props.handleChange} />
    </div>
  );
}

export default function NameInput() {
  const [first, setFirst] = React.useState('')
  const [last, setLast] = React.useState('')

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Input
        value={first}
        label="First"
        handleChange={(e) => setFirst(e.target.value)}
      />
      <Input
        value={last}
        label="Last"
        handleChange={(e) => setLast(e.target.value)}
      />
    </div>
    );
}
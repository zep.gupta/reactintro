import * as React from "react";

export default class Counter extends React.Component {
  state = {
    count: 0
  };

  updateCount = num => {
    this.setState({ count: this.state.count + num });
  };

  render() {
    return (
      <div>
        <span>{this.props.label}</span>
        <button onClick={() => this.updateCount(1)}>increment</button>
        <p style={{ color: "white" }}>{this.state.count}</p>
        <button onClick={() => this.updateCount(-1)}>decrement</button>
      </div>
    );
  }
}

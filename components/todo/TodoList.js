import React from "react";
import TodoItems from "./TodoItems";
import TodoInput from "./TodoInput";

export default class TodoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { todos: this.props.todos || [] };
  }
  addTodo = item => {
    this.setState({ todos: this.state.todos.concat([item]) });
  };

  render() {
    return (
      <div>
        <h3>TODO LIST</h3>
        <TodoItems items={this.state.todos} />
        <TodoInput addTodo={this.addTodo} />
      </div>
    );
  }
}

import React from "react";

export default function TodoItems(props) {
  const createItem = (item, index) => {
    return <li key={index}>{item}</li>;
  };
  return <ul style={{ color: "white" }}>{props.items.map(createItem)}</ul>;
}

import * as React from "react";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import TextField from "@material-ui/core/TextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

export default class MaterialUIEx extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 0
    };
  }

  render() {
    const bull = <span>•</span>;

    return (
      <div style={{ display: "flex", flexDirection: "column" }}>
        <Stepper activeStep={this.state.step} orientation="vertical" label="Stepper Component">
          <Step>
            <StepLabel>Input Component</StepLabel>
            <StepContent>
              <TextField label="Name" margin="normal" variant="outlined" />
            </StepContent>
          </Step>
          <Step>
            <StepLabel>Progress Component</StepLabel>
            <StepContent>
              <CircularProgress />
            </StepContent>
          </Step>
          <Step>
            <StepLabel>Card Component</StepLabel>
            <StepContent>
              <Card>
                <CardContent>
                  <Typography color="textSecondary" gutterBottom>
                    Word of the Day
                  </Typography>
                  <Typography variant="h5" component="h2">
                    be
                    {bull}
                    nev
                    {bull}o{bull}
                    lent
                  </Typography>
                  <Typography color="textSecondary">adjective</Typography>
                  <Typography component="p">
                    well meaning and kindly.
                    <br />
                    {'"a benevolent smile"'}
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size="small">Learn More</Button>
                </CardActions>
              </Card>
            </StepContent>
          </Step>
        </Stepper>
        <div style={{ display: "flex" }}>
          <Button
            variant="contained"
            color="primary"
            onClick={() =>
              this.setState({
                step: this.state.step === 0 ? this.state.step : this.state.step - 1
              })
            }
          >
            back
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() =>
              this.setState({
                step: this.state.step === 2 ? this.state.step : this.state.step + 1
              })
            }
          >
            next
          </Button>
        </div>
      </div>
    );
  }
}
